/* use rsr19a;	-- (i.e., your fsuid)
show tables;
select * from benefit;
select * from dependent;
select * from emp_hist;
select * from employee;
select * from job;
select * from plan;
#select * from publisher;
*/

/* A character set is a set of symbols and encodings.
A collation is a set of rules for comparing characters in a character set.
Suppose that we have an alphabet with four letters: "A", "B", "a", "b".
We give each letter a number: "A" = 0, "B" = 1, "a" = 2, "b" = 3.
The letter "A" is a symbol, the number 0 is the encoding for "A",
and the combination of all four letters and their encodings is a character set.
Suppose that we want to compare two string values, "A" and "B".
The simplest way to do this is to look at the encodings: 0 for "A" and 1 for "B".
Because O is less than 1, we say "A" is less than "B". What we've just done is apply a collation to our character set.
The collation is a set of rules (only one rule in this case): "compare the encodings."
We call this simplest of all possible collations a binary collation.
http://dev.mysql.com/doc/refman/5.5/en/charset.html
*/

-- set foreign_key_checks=0;

drop database if exists rsr19a;
create database if not exists rsr19a;
use rsr19a;

-- -----------------------------------
-- Table company
-- -----------------------------------

DROP TABLE IF EXISTS company;
CREATE TABLE IF NOT EXISTS company
(
cmp_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
cmp_type enum('C-Corp','S-Corp','Non-Profit-Corp', 'LLC','Partnership'),
cmp_street VARCHAR(30) NOT NULL,
cmp_city VARCHAR(30) NOT NULL,
cmp_state CHAR(2) NOT NULL,
cmp_zip int(9) unsigned ZEROFILL NOT NULL COMMENT 'no dashes',
cmp_phone bigint unsigned NOT NULL COMMENT 'ssn and zip codes can be zero-filled, but not US area codes',
cmp_ytd_sales DECIMAL(10,2) unsigned NOT NULL COMMENT '12,345,678.90',
cmp_email VARCHAR(100) NULL,
cmp_url VARCHAR(100) NULL,
cmp_notes VARCHAR(255) NULL,
PRIMARY KEY (cmp_id)
)

ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

INSERT INTO company
VALUES
(null,'Non-Profit-Corp','8240 Pennington Ave','Frederick','MD','217018798','7864567896','8974637.00',null,'http://www.http://Marvel.com','Avengers enrolled'),
(null,'C-Corp','762 Ridge Ave','Phoenix','AZ','850219876','3056789867','32145678.00',null,'http://www.DC.com','Superman Agaisnt'),
(null,'LLC','899 Riverview Street','Roselle','IL','601727645','7869576893','567864.00',null,'http://www.Disney.com','Bought Marvel'),
(null,'Partnership', '86 Rocky River St','Lockport','NY','140947568','8508675936','8746357.00',null,'http://www.Netflix.com','company notes4'),
(null,'S-Corp', '9261 Pennington Drive','Greenwood','SC','296468769','7865647823','7464734.00',null,'http://www.TAG.com','company notes5');

SHOW WARNINGS;

-- ---------------------------
-- Table customer
-- ---------------------------

DROP TABLE IF EXISTS customer;
CREATE TABLE IF NOT EXISTS customer
(
cus_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
cmp_id INT UNSIGNED NOT NULL,
cus_ssn binary(64) not null,
cus_salt binary(64) not null COMMENT '*only* demo purposes - do *NOT* use *salt* in the name!',
cus_type enum('Loyal', 'Discount','Impulse','Need-Based','Wandering'),
cus_first VARCHAR(15) NOT NULL,
cus_last VARCHAR(30) NOT NULL,
cus_street VARCHAR(30) NULL,
cus_city VARCHAR(30) NULL,
cus_state CHAR(2) NULL,
cus_zip int(9) unsigned ZEROFILL NULL,
cus_phone bigint unsigned NOT NULL COMMENT 'ssn and zip codes can be zero-filled, but not US area codes',
cus_email VARCHAR(100) NULL,
cus_balance DECIMAL(7.2) unsigned NULL COMMENT '12,345.67',
cus_tot_sales DECIMAL(7,2) unsigned NULL,
cus_notes VARCHAR(255) NULL,
PRIMARY KEY (cus_id),
UNIQUE INDEX ux_cus_ssn (cus_ssn ASC),
INDEX idx_cmp_id (cmp_id ASC),

/*
Comment CONSTRAINT line to demo DBMS auto value when *not* using "constraint" option for foreign keys, then...
SHOW CREATE TABLE customer;
*/

CONSTRAINT fk_customer_company
FOREIGN KEY (cmp_id)
REFERENCES company (cmp_id)
ON DELETE NO ACTION
ON UPDATE CASCADE
)

ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

-- salting and hashing sensitive data (e.g., SSN). Normally, *each* record would receive unique random salt!

set @salt=RANDOM_BYTES(64);

INSERT INTO customer

VALUES
(null,2,unhex(SHA2(CONCAT(@salt, 000456789),512)),@salt, 'Loyal','Rafael', 'Rodriguez','8869 Nw 7th St', 'Miami', 'FL','331728976', '7863267865', 'rsr69@mymail.com','8391.87','37642.00','customer notes'),
(null,4,unhex(SHA2(CONCAT(@salt, 001456789),512)),@salt, 'Discount','Natally', 'Avila','425 Sw 122nd Ave','Miami', 'FL','331847865','3057864444', 'ngag2@mymail.com','675.57','87341.00','customer notes2'),
(null,3,unhex(SHA2(CONCAT(@salt, 002456789),512)),@salt,'Wandering','Raymond','Cano', '9876 fontainebleau blvd','Miami', 'FL','331267876', '7868008898', 'Raymen@mymail.com','8730.23','92678.00','customer notes 3'),
(null,5,unhex(SHA2(CONCAT(@salt, 003456789),512)),@salt, 'Need-Based','Gabriel', 'Gomez','1817 W call St','Tallahassee', 'FL','323047865', '8507863056', 'gga@mymail.com', '2651.19','78345.00','customer notes4'),
(null,1,unhex(SHA2(CONCAT(@salt, 004456789),512)),@salt,'Impulse','Angie','Rodriguez','8075 Nw 8 St', 'Miami', 'FL','303326789','7864447867', 'anste@mymail.com','782.73', '23471.00','customer notes5');

SHOW WARNINGS;

-- set foreign_key_checks=1;

select * from company;
select * from customer;
