-- set foreign_key_checks=1;

select * from company;
select * from customer;


-- 1. Limit user1 to select, update, and delete priviledges on company and customer tables

/* 
Two-step process:
Create new user with CREATE USER statement, then use GRANT statement:
1) CREATE USER IF NOT EXISTS 'user1'@'localhost' IDENTIFIED BY 'password';
2) Grant select, update, delete databasename.company to 'username'@'localhost';
*/

CREATE USER IF NOT EXISTS 'user1'@'localhost' IDENTIFIED BY 'test1';
CREATE USER IF NOT EXISTS 'user2'@'localhost' IDENTIFIED BY 'test2';
flush privileges;

-- NOTE: *MUST* flush privileges after creating or removing users and /or privileges!

-- check
use mysql;
select * from user; 

-- also, can drop users; 
drop user if exists someuser;
drop user if exists 'someuser'@'localhost'
flush privileges;

-- Next: Grant permissions: 
Grant select, update, delete
on rsr19a.customer
to user1@"localhost";

Grant select, update, delete
on rsr19a.company
to user1@"localhost";

SHOW WARNINGS;

-- 2. Limit user2 to select, and insert privileges on customer table
Grant select, insert
on rsr19a.customer
to user2@'localhost';
show warnings;

flush privileges;

-3. show grants for you, user1, user2 (must log in as each user)
-- a. yours/admin: show grants;
-- b. user1(logged in as user1): show grants;
-- c. user2 (logged in as admin): show grants for 'user2'@'localhost';

-- Note: can show individual user priveleges as Admin
-- Example: SHOW GRANTS FOR 'user1'@'localhost';


1. show grants;
2. Show grants for 'user2'@'localhost';
3. Show grants for 'user1'@'localhost';

4. select user(), version()

