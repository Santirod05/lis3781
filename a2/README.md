> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions.

## Rafael Rodriguez

### Assignment #2 Requirements:

*Deliverables:*
*Three Parts:*

1. Creating tables, database, and insert statements using MySQL Server.
2. Granting Privileges to different users on server.
3. Populating data and pushing to both local and cloud server.

#### README.md file should include the following items:

* Screenshot of assignment 2 SQL Code
* Screenshot of assignment 2 populated tables

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

#### a2 SQL code 
| Company | Customer |
|-----|-----|
| ![Company](img/sql_code.png) | ![Customer](img/sql_code2.png) |

| a2 Populated Tables |
| ----- |
| ![Tables](img/a2_populated_tables.png) |

#### Granting Privileges for user1 and user2 and admin 

| User 1 | User 2 |
| ------- | ------- |
| ![User1](img/user1.png) | ![User2](img/user2.png) |
|       Admin     |
| --------------- |
| ![MySQL Version](img/admin.png) |
