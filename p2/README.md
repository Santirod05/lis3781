> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions.

## Rafael Rodriguez

### Project 2 Requirements:

*Deliverables:*
*Three Parts:*

1. Installation for Mongodb and importation
2. Introduction to NoSQL DBMS and MongoDB
3. Required reports and JSON Code Solution

#### README.md file should include the following items:
 
#### Assignment requirements.
- Screenshot of at least one MongoDB shell command(s), (e.g., show collections);
- Screenshot: At least *one* required report (i.e., exercise below), and JSON code solution.
- Bitbucket repo links: *Your* lis3781 Bitbucket repo link

### Assignment Screenshot and Links:

#### Screenshots of Mongodb Required reports:
|    Mongodb Required report 1   |     Mongodb Required report 2    |
| ------------------------------------------- | ------------------------------------ |
| ![Mongodb Required reports Screenshot](img/reports.png "reports 1 Screenshot") | ![Mongodb Required reports Screenshot 2](img/reports2.png "reports 2 Screenshot") |

#### Screenshots of JSON Code Solution:

|    JSON Code Solution 1    |     JSON Code Solution 2    |
| ------------------------------------------- | ------------------------------------ |
| ![JSON Code Solution 1 Screenshot](img/json.png "JSON Code Solution 1 Screenshot") | ![JSON Code Solution 2 Screenshot](img/json2.png "JSON Code Solution 2 Screenshot") |

#### Screenshot of MongoDB shell command(s):

![Screenshot of P2 program running on R Studio](img/shell_com.png "Scrrenshot of P2 program running on R Studio")