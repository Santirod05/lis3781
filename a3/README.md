> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 Advanced Database Management.

## Rafael Rodriguez

### Assignment #3 Requirements:

*Deliverables:*
*Three Parts:*

1. Download, Install, and operate Microsoft Remote Desktop to run Oracle Server
2. Create Customer, Commodity, and Order tables on Oracle Server
3. Create and Excute SQL Queries to retrieve specified information

#### README.md file should include the following items:

* Screenshot of *your* SQL code used to create and populate your tables; 
* Screenshot of *your* populated tables (w/in the Oracle environment); 
* Optional: SQL code for a few of the required reports. 
* Bitbucket repo links: *Your* lis3781 Bitbucket repo link 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

#### a3 Oracle SQL code 

| Oracle code 1 | Oracle code 2 | Oracle code 3
| ----- | ----- | ----- |
| ![Code 1](img/sql_code1.png) | ![Code 2](img/sql_code2.png) |![Code 3](img/sql_code3.png) |

#### a3 Oracle Environment & a3 Populated Tables

| Oracle Environment | Populated Tables |
| ---------- | ---------- |
| ![Oracle Env](img/sql_env.png) | ![a3 Tables](img/sql_tables.png) |

#### Detailed a3 Populated Tables

| Customer Table| Commodity Table | Order Table |
| ------- | ------- | ------- |
| ![Customer](img/cus_tab.png) | ![Commodity](img/com_tab.png) | ![Order](img/order_tab.png)


