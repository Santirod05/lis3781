> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 Advanced Database Management.

## Rafael Rodriguez

### Project #1 Requirements:

*Deliverables:*
*Three Parts:*

1. Use of MySQL server to create local city court case database.
2. Using SHA2 in order to protect sensetive information i.e. SSN's.
3. Creation of stored procedures to salt and hash information.


#### README.md file should include the following items:

* Screenshot of Project 1 ERD.
* mySQL code for the Project 1.
* Screenshot of populated tables.

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

#### Project 1 ERD 

![P1 ERD](img/p1_erd.png)

#### Project 1 mySQL code 

| mySQL code 1 | mySQL code 2 |
| ------------ | ------------ |
| ![mySQL Code 1](img/mysql_code1.png) | ![mySQL Code 2](img/mysql_code2.png) |

#### Project 1 Populated Tables

| Project 1 mySQL | Project 1 mySQL |
| --------------- | --------------- |
| ![p1 tables](img/sql_tab1.png) | ![p1 tables](img/sql_tab2.png) |