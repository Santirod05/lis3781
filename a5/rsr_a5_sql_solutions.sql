use master;
GO

if exists (select name from master.dbo.sysdatabases where name = N'rsr19a')
drop database rsr19a;
GO

if not exists (select name from master.dbo.sysdatabases where name = N'rsr19a')
create database rsr19a;
GO

use rsr19a;
GO

-- -------------------------------------
-- Table person
-- -------------------------------------

if OBJECT_ID (N'dbo.person', N'U') is not null
drop table dbo.person;
GO

create table dbo.person(
    per_id smallint not null identity(1,1),
    per_ssn binary(64) null,
    per_salt binary(64) null,
    per_fname varchar(15) not null,
    per_lname varchar(30) not null,
    per_gender char(1) not null check (per_gender IN('m','f')),
    per_dob date not null,
    per_street varchar(30) not null,
    per_city varchar(30) not null,
    per_state char(2) not null default 'FL',
    per_zip int not null check (per_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    per_email varchar(100) null,
    per_type char(1) not null check (per_type in('c','s')),
    per_notes varchar(45) null,
    primary key (per_id),

    constraint ux_per_ssn unique nonclustered (per_ssn ASC)
);
GO

-- -------------------------------------
-- Table phone
-- -------------------------------------
if OBJECT_ID (N'dbo.phone', N'U') is not null
drop table dbo.phone;
GO

create table dbo.phone(
    phn_id smallint not null identity(1,1),
    per_id smallint not null,
    phn_num bigint not null check (phn_num like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    phn_type char(1) not null check (phn_type in('h','c','w','f')),
    phn_notes varchar(255) null,
    primary key (phn_id),

    constraint fk_phone_person
    foreign key (per_id)
    references dbo.person (per_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table customer
-- -------------------------------------
if OBJECT_ID (N'dbo.customer', N'U') is not null
drop table dbo.customer;
GO

create table dbo.customer(
    per_id smallint not null,
    cus_balance decimal(7,2) not null check (cus_balance >= 0),
    cus_total_sales decimal(7,2) not null check (cus_total_sales >= 0),
    cus_notes varchar(45) null,
    primary key (per_id),

    constraint fk_customer_person
    foreign key (per_id)
    references dbo.person (per_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table slsrep
-- -------------------------------------
if OBJECT_ID (N'dbo.slsrep', N'U') is not null
drop table dbo.slsrep;
GO

create table dbo.slsrep(
    per_id smallint not null,
    srp_yr_sales_goal decimal(8,2) not null check (srp_yr_sales_goal >= 0),
    srp_ytd_sales decimal(8,2) not null check (srp_ytd_sales >= 0),
    srp_ytd_comm decimal(7,2) not null check (srp_ytd_comm >= 0),
    srp_notes varchar(45) null,
    primary key (per_id),

    constraint fk_slsrep_person
    foreign key (per_id)
    references dbo.person (per_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table srp_hist
-- -------------------------------------

if OBJECT_ID (N'dbo.srp_hist', N'U') is not null
drop table dbo.srp_hist;
GO

create table dbo.srp_hist(
    sht_id smallint not null identity(1,1),
    per_id smallint not null,
    sht_type char(1) not null check (sht_type in('i','u','d')),
    sht_modified datetime not null,
    sht_modifier varchar(45) not null default system_user,
    sht_date date not null default getDate(),
    sht_yr_sales_goal decimal(8,2) not null check (sht_yr_sales_goal >= 0),
    sht_ytd_sales decimal(8,2) not null check (sht_ytd_sales >= 0),
    sht_ytd_comm decimal(7,2) not null check (sht_ytd_comm >= 0),
    sht_notes varchar(45) null,
    primary key (sht_id),

    constraint fk_srp_hist_slsrep
    foreign key (per_id)
    references dbo.slsrep (per_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table contact
-- -------------------------------------
if OBJECT_ID (N'dbo.contact', N'U') is not null
drop table dbo.contact;
GO

create table dbo.contact(
    cnt_id int not null identity(1,1),
    per_cid smallint not null,
    per_sid smallint not null,
    cnt_date datetime not null,
    cnt_notes varchar(255) null,
    primary key (cnt_id),

    constraint fk_contact_customer
    foreign key (per_cid)
    references dbo.customer (per_id)
    on delete cascade
    on update cascade,
    
    constraint fk_contact_slsrep
    foreign key (per_sid)
    references dbo.slsrep (per_id)
    on delete no action
    on update no action
);
GO

-- -------------------------------------
-- Table [order]
-- -------------------------------------
if OBJECT_ID (N'dbo.[order]', N'U') is not null
drop table dbo.[order];
GO

create table dbo.[order](
    ord_id int not null identity(1,1),
    cnt_id int not null,
    ord_placed_date datetime not null,
    ord_filled_date datetime null,
    ord_notes varchar(255) null,
    primary key (ord_id),

    constraint fk_order_contact
    foreign key (cnt_id)
    references dbo.contact (cnt_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table region
-- -------------------------------------

if OBJECT_ID (N'dbo.region', N'U') is not null
drop table dbo.region;
GO

create table dbo.region(
    reg_id tinyint not null identity(1,1),
    reg_name char(1) not null check (reg_name IN('n', 'e', 's', 'w', 'c')), -- n,e,s,w,c (north, east, south, west, central)
    reg_notes varchar(255) null,
    primary key (reg_id)
);
GO

-- -------------------------------------
-- Table state
-- -------------------------------------

if OBJECT_ID (N'dbo.state', N'U') is not null
drop table dbo.state;
GO

create table dbo.state(
    ste_id tinyint not null identity(1,1),
    reg_id tinyint not null,
    ste_name char(2) not null default 'FL',
    ste_notes varchar(255) null,
    primary key (ste_id),

    constraint fk_state_region
    foreign key (reg_id)
    references dbo.region (reg_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table city 
-- -------------------------------------

if OBJECT_ID (N'dbo.city', N'U') is not null
drop table dbo.city;
GO

create table dbo.city(
    cty_id smallint not null identity(1,1),
    ste_id tinyint not null,
    city_name varchar(30) not null,
    city_notes varchar(255) null,
    primary key (cty_id),

    constraint fk_city_state
    foreign key (ste_id)
    references dbo.state (ste_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table store
-- -------------------------------------
if OBJECT_ID (N'dbo.store', N'U') is not null
drop table dbo.store;
GO

create table dbo.store(
    str_id smallint not null identity(1,1),
    cty_id smallint not null,
    str_name varchar(45) not null,
    str_street varchar(30) not null,
    str_city varchar(30) not null,
    str_state char(2) not null default 'FL',
    str_zip int not null check (str_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_phone bigint not null check (str_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_email varchar(100) not null,
    str_url varchar(100) not null,
    str_notes varchar(255) null,
    primary key (str_id),

    constraint fk_store_city
    foreign key (cty_id)
    references dbo.city (cty_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table invoice
-- -------------------------------------
if OBJECT_ID (N'dbo.invoice', N'U') is not null
drop table dbo.invoice;
GO

create table dbo.invoice(
    inv_id int not null identity(1,1),
    ord_id int not null,
    str_id smallint not null,
    inv_date datetime not null,
    inv_total decimal(8,2) not null check (inv_total >= 0),
    inv_paid bit not null,
    inv_notes varchar(255) null,
    primary key (inv_id),

    constraint ux_ord_id unique nonclustered (ord_id asc),

    constraint fk_invoice_order
    foreign key (ord_id)
    references dbo.[order] (ord_id)
    on delete cascade
    on update cascade, 

    constraint fk_invoice_store
    foreign key (str_id)
    references dbo.store (str_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table payment
-- -------------------------------------
if OBJECT_ID (N'dbo.payment', N'U') is not null
drop table dbo.contact;
GO

create table dbo.payment(
    pay_id int not null identity(1,1),
    inv_id int not null,
    pay_date datetime not null,
    pay_amt decimal(7,2) not null check (pay_amt >= 0),
    pay_notes varchar(255) null,
    primary key (pay_id),

    constraint fk_payment_invoice
    foreign key (inv_id)
    references dbo.invoice (inv_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table vendor
-- -------------------------------------
if OBJECT_ID (N'dbo.vendor', N'U') is not null
drop table dbo.vendor;
GO

create table dbo.vendor(
    ven_id smallint not null identity(1,1),
    ven_name varchar(45) not null,
    ven_street varchar(30) not null,
    ven_city varchar(30) not null,
    ven_state char(2) not null default 'FL',
    ven_zip int not null check (ven_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_phone bigint not null check (ven_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_email varchar(100) not null,
    ven_url varchar(100) not null,
    ven_notes varchar(255) null,
    primary key (ven_id)
);
GO

-- -------------------------------------
-- Table product
-- -------------------------------------
if OBJECT_ID (N'dbo.product', N'U') is not null
drop table dbo.product;
GO

create table dbo.product(
    pro_id smallint not null identity(1,1),
    ven_id smallint not null,
    pro_name varchar(30) not null,
    pro_descript varchar(45) null,
    pro_weight float not null check (pro_weight >= 0),
    pro_qoh smallint not null check (pro_qoh >= 0),
    pro_cost decimal(7,2) not null check (pro_cost >= 0),
    pro_price decimal(7,2) not null check (pro_price >= 0),
    pro_discount decimal(3,0) null,
    pro_notes varchar(255) null,
    primary key (pro_id),

    constraint fk_product_vendor
    foreign key (ven_id)
    references dbo.vendor (ven_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table product_hist
-- -------------------------------------
if OBJECT_ID (N'dbo.product_hist', N'U') is not null
drop table dbo.product_hist;
GO

create table dbo.product_hist(
    pht_id smallint not null identity(1,1),
    pro_id smallint not null,
    pht_date datetime not null,
    pht_cost decimal(7,2) not null check (pht_cost >= 0),
    pht_price decimal(7,2) not null check (pht_price >= 0),
    pht_discount decimal(3,0) null,
    pht_notes varchar(255) null,
    primary key (pht_id),

    constraint fk_product_hist_product
    foreign key (pro_id)
    references dbo.product (pro_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table order_line
-- -------------------------------------
if OBJECT_ID (N'dbo.order_line', N'U') is not null
drop table dbo.order_line;
GO

create table dbo.order_line(
    oln_id int not null identity(1,1),
    ord_id int not null,
    pro_id smallint not null,
    oln_qty smallint not null check (oln_qty >= 0),
    oln_price decimal(7,2) not null check (oln_price >= 0),
    oln_notes varchar(255) null,
    primary key (oln_id),

    constraint fk_order_line_order
    foreign key (ord_id)
    references dbo.[order] (ord_id)
    on delete cascade
    on update cascade,

    constraint fk_order_line_product
    foreign key (pro_id)
    references dbo.product (pro_id)
    on delete cascade
    on update cascade
);
GO

-- -------------------------------------
-- Table time
-- -------------------------------------

IF OBJECT_ID (N'dbo.time', N'U') IS NOT NULL
DROP TABLE dbo.time;
GO

CREATE TABLE dbo.time(
    tim_id INT NOT NULL identity(1,1),
    tim_yr SMALLINT NOT NULL, -- 2 byte integer (no YEAR data type in MS SQL Server)
    tim_qtr TINYINT NOT NULL, -- 1 - 4 
    tim_month TINYINT NOT NULL, -- 1 - 12 
    tim_week TINYINT NOT NULL, -- 1 - 52
    tim_day TINYINT NOT NULL, -- 1 - 7
    tim_time TIME NOT NULL, -- based on 24-hour clock
    tim_notes VARCHAR(255) NULL,
    PRIMARY KEY (tim_id)
);
GO

-- -------------------------------------
-- Table sale
-- -------------------------------------

IF OBJECT_ID (N'dbo.sale', N'U') IS NOT NULL
DROP TABLE dbo.sale;
GO

CREATE TABLE dbo.sale(
    pro_id SMALLINT NOT NULL,
    str_id SMALLINT NOT NULL,
    cnt_id INT NOT NULL,
    tim_id INT NOT NULL,
    sal_qty SMALLINT NOT NULL,
    sal_price DECIMAL(8,2) NOT NULL,
    sal_total DECIMAL(8,2) NOT NULL,
    sal_notes VARCHAR(255) NULL,
    PRIMARY KEY (pro_id, cnt_id, tim_id, str_id),

    CONSTRAINT ux_pro_id_str_id_cnt_id_tim_id
    unique nonclustered (pro_id ASC, str_id ASC, cnt_id ASC, tim_id ASC),

    CONSTRAINT fk_sale_time
    FOREIGN KEY (tim_id)
    REFERENCES dbo.time (tim_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT fk_sale_contact
    FOREIGN KEY (cnt_id)
    REFERENCES dbo.contact (cnt_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT fk_sale_store
    FOREIGN KEY (str_id)
    REFERENCES dbo.store (str_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT fk_sale_product
    FOREIGN KEY (pro_id)
    REFERENCES dbo.product (pro_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

Select * from information_schema.tables;

-- -------------------------------------
-- DATA person
-- -------------------------------------
insert into dbo.person
(per_ssn, per_salt, per_fname, per_lname, per_gender, per_dob, per_street, per_city, per_state, per_zip, per_email, per_type, per_notes)
values
(1, NULL, 'Rafael', 'Rodriguez', 'm', '1969-06-05', '8075 nw 8th st.', 'Miami', 'FL', 331726522, 'rsr19@my.fsu.com', 'c', NULL), 
(2, NULL, 'Santi', 'Rodriguez', 'm', '1989-09-03', '123 Main rd.', 'Tallahassee', 'FL', 323208440, 'Santirod@live.net', 'c', NULL), 
(3, NULL, 'Natally', 'Avila', 'f', '1976-10-25', '12 E Main Ave.', 'Orlando', 'FL', 331862341, 'Natavil©hotmail.coml', 'c', NULL), 
(4, NULL, 'Raymond', 'Cano', 'm', '1999-08-01', '123 Sea Street Ave.', 'Palm Beach', 'FL', 331284409, 'RaymanCano©gmail.coml', 's', NULL), 
(5, NULL, 'Sabrina', 'Garcia', 'f', '1996-02-29', '24 E Smith Rd.', 'Miami', 'FL', 312234178, 'Sabcyrus©gmail.net', 's', NULL), 
(6, NULL, 'Natalie', 'Samara', 'f', '1994-03-16', '45 Skyview Ave.', 'Ft. Lauderdale', 'FL', 302638332, 'nsamara©yahoo.com', 'c', NULL), 
(7, NULL, 'Nohora', 'Amaya', 'f', '1991-03-12', '25 Dewey St.', 'Jacksonvile', 'FL', 324348890, 'santiangie©gmail.com', 'c', NULL), 
(8, NULL, 'Angie', 'Rodriguez', 'f', '1989-06-10', '8075 nw 7th st', 'Miami', 'FL', 321638332, 'anstero@yahoo.com', 'c', NULL), 
(9, NULL, 'Daniel', 'Menacious', 'm', '1970-03-22', '9199 Fontainebleau Blvd', 'Miami', 'FL', 367348890, 'danimena@gmail.com', 'c', NULL), 
(10, NULL, 'Justin', 'Ortega', 'm', '1983-12-24', '8095 nw 8th st', 'Miami', 'FL', 323638332, 'justinooo©hotmail.com', 'c', NULL), 
(11, NULL, 'Lebron', 'James', 'm', '1975-12-15', '9199 Fontainebleau Blvd', 'Miami', 'FL', 331219932, 'LBJ©gmail.con', 'c', NULL), 
(12, NULL, 'Radamel', 'Falcao', 'm', '1980-08-22', '9199 Fontainebleau Blvd', 'Miami', 'FL', 312048823, 'eltigre@hotmail.com', 'c', NULL), 
(13, NULL, 'Anthony', 'Parra', 'm', '1995-01-31', '8275 nw 9th st', 'Miami', 'FL', 332219932, 'anthonyparra@live.com', 'c', NULL), 
(14, NULL, 'Sebastian', 'Vega', 'm', '1970-03-22', '8888 W 88 Ave.', 'Gulf Pines', 'FL', 313048823, 'sebavega@gmail.com', 'c', NULL), 
(15, NULL, 'Logan', 'Tuna', 'm', '1982-06-13', '33 Gay st.', 'Miami', 'FL', 323219932, 'tunalogan@aol.com', 'c', NULL); 
GO 

Select * from person;

-- -------------------------------------
-- DATA phone
-- -------------------------------------
insert into dbo.phone
( per_id, phn_num, phn_type, phn_notes)
values
(5, 3054917385, 'h', null), 
(4, 7863266089, 'h', null), 
(3, 8501234567, 'h', null), 
(2, 3056748769, 'h', null), 
(1, 7864446789, 'h', null);
GO 

Select * from phone;

-- -------------------------------------
-- DATA slsrep
-- -------------------------------------
insert into dbo.slsrep
(per_id, srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes)
values
(1, 12050, 60000, 1800, null), 
(2, 60680, 35000, 3500, 'needs to pick up sales by 15%'), 
(3, 157689, 84000, 9650, '#1 salesperson'), 
(4, 125000, 87000, 15300, null), 
(5, 98000, 43000, 8750, null);
GO 

Select * from slsrep;

-- -------------------------------------
-- DATA customer
-- -------------------------------------
insert into dbo.customer
(per_id, cus_balance, cus_total_sales, cus_notes)
values
(6, 320, 36778, null), 
(7, 38.45, 134.92, 'newest member to the team'), 
(8, 20, 6530, null), 
(9, 781.73, 3450.92, null), 
(10, 221.23, 666.57, null);
GO 

Select * from customer;

-- -------------------------------------
-- DATA contact
-- -------------------------------------
insert into dbo.contact
(per_sid, per_cid, cnt_date, cnt_notes)
values
(1, 6, '1989-10-05', null), 
(2, 7, '2018-07-06', null), 
(3, 8, '2012-03-09', null), 
(4, 9, '2001-01-03', null), 
(5, 10, '2002-09-08', null);
GO 

Select * from contact;

-- -------------------------------------
-- DATA [order]
-- -------------------------------------
insert into dbo.[order]
(cnt_id, ord_placed_date, ord_filled_date, ord_notes)
values
(1, '2021-10-09', '2021-12-09', null), 
(2, '2016-11-02', '2017-01-02', null), 
(3, '2011-04-07', '2011-07-07', null), 
(4, '2006-09-04', '2010-11-20', 'Package was lost at sea'), 
(5, '2001-03-04', '2001-06-05', null);
GO 

Select * from [order];

-- -------------------------------------
-- DATA for table region
-- -------------------------------------
INSERT INTO region
(reg_name, reg_notes)
VALUES
('w', NULL),
('s', NULL),
('e', NULL),
('n', NULL),
('c', NULL);
GO

select * from dbo.region;

-- -------------------------------------
-- DATA for table state
-- -------------------------------------
INSERT INTO state
(reg_id, ste_name, ste_notes)
VALUES
(1, 'FL', NULL),
(3, 'MI', NULL),
(4, 'IL', NULL),
(5, 'WA', NULL),
(2, 'LA', NULL);
GO

select * from dbo.state;

-- -------------------------------------
-- DATA for table city
-- -------------------------------------
INSERT INTO city
(ste_id, city_name, city_notes)
VALUES
(1, 'Miami', NULL),
(2, 'Detroit', NULL),
(2, 'Clover', NULL),
(3, 'Chicago', NULL),
(4, 'St. Louis', NULL);
GO

select * from dbo.city;

-- -------------------------------------
-- DATA store
-- -------------------------------------
insert into dbo.store
(cty_id, str_name, str_street, str_city, str_state, str_zip, str_phone, str_email, str_url, str_notes)
values
(2, 'Truelieve', '300 S 42nd Ave', 'Miami', 'FL', 331315690, 7863264444, 'TL@hotmail.com', 'https://www.truelieve.com', null), 
(3, 'Grow Healthy', '200 W 43 St.', 'Ft. Lauderdale', 'FL', 323231519, 9546789870, 'GrowHealthy@live.com', 'https://www.GrowHealthy.com', null), 
(4, 'MedMen', '500 N Capitol Rd.', 'Orlando', 'FL', 331345671, 3058007698, 'MedMen@gmail.com', 'https://www.MedMen.com', null), 
(5, 'Cookies', '420 Smokey Blvd.', 'Jacksonville', 'FL', 323271892, 7865128999, 'Bcookies@live.com', 'https://www.cookies.com', null), 
(1, 'KK', '900 S Monroe St.', 'Tallahassee', 'FL', 331233456, 3056061234, 'Khalifatree@live.com', 'https://www.tayloredgang.com', null);
GO 

Select * from store;

-- -------------------------------------
-- DATA invoice
-- -------------------------------------
insert into dbo.invoice
(ord_id, str_id, inv_date, inv_total, inv_paid, inv_notes)
values
(5, 1, '2008-06-05', 188.32, 0, null), 
(4, 1, '2020-11-10', 350.059, 0, null), 
(3, 2, '2008-09-06', 200.34, 1, null), 
(2, 4, '2018-01-10', 699.32, 1, null), 
(1, 5, '2021-06-24', 5609.67, 0, null);
GO 

Select * from invoice;

-- -------------------------------------
-- DATA vendor
-- -------------------------------------
insert into dbo.vendor
(ven_name, ven_street, ven_city, ven_state, ven_zip, ven_phone, ven_email, ven_url, ven_notes)
values
('AuroraDispo', '43 deep drive South', 'Ft. Lauderdale', 'FL', 475315690, 7867658127, 'sales@AuroraDispo.com', 'https://www.AuroraDispo.com', null), 
('General Eletric', '999 Ghost Rd', 'Seattle', 'WA', 505231519, 3058926534, 'sales@ge.com', 'https://www.ge.com', null), 
('NativeRoots', '33 Gay st.', 'Miami', 'FL', 802345671, 7867653421, 'sales@NativeRoots.com', 'https://www.NativeRoots.com', null), 
('MedMenDispo', '56 S 23rd Ave.', 'Miami', 'FL', 372271892, 3052718923, 'info@MedMenDispo.com', 'https://www.MedMenDispo.com', null), 
('Truelieve Dispo', '8888 W 88 Ave.', 'Gulf Pines', 'FL', 954983456, 3137583492, 'sales@TLDispo.com', 'https://www.TLDispo.com', null);
GO 

Select * from vendor;

-- -------------------------------------
-- DATA product
-- -------------------------------------
insert into dbo.product
(ven_id, pro_name, pro_descript, pro_weight, pro_qoh, pro_cost, pro_price, pro_discount, pro_notes)
values
(1, 'Forbidden Fruit', null, 2.5, 45, 4.99, 7.99, 30, null), 
(2, 'Peanut Butter Breathe', null, 1.8, 120, 1.99, 3.19, null, null), 
(3, 'Khalifa Kush', null, 2.8, 48, 3.89, 7.99, 40, null), 
(4, 'Grand Daddy Purp', null, 15, 19, 19.99, 28.99, null, 'Pounds'), 
(5, 'Train Wreck', null, 3.5, 178, 8.45, 13.99, 50, null);
GO 

Select * from product;

-- -------------------------------------
-- DATA order_line
-- -------------------------------------
insert into dbo.order_line
(ord_id, pro_id, oln_qty, oln_price, oln_notes)
values
(1, 2, 10, 800.0, null), 
(2, 3, 7, 950.88, null), 
(3, 4, 3, 6000.99, null), 
(5, 1, 2, 12000.76, null), 
(4, 5, 13, 5800.99, null);
GO 

Select * from order_line;

-- -------------------------------------
-- DATA payment
-- -------------------------------------
insert into dbo.payment
(inv_id, pay_date, pay_amt, pay_notes)
values
(5, '2020-08-20', 70.99, null), 
(4, '2018-07-18', 95.99, null), 
(1, '1999-06-23', 200.75, null), 
(3, '2002-02-28', 1000.55, null), 
(2, '2007-08-29', 3250.5, null);
GO 

Select * from payment;

-- -------------------------------------
-- DATA product_hist
-- -------------------------------------
insert into dbo.product_hist
(pro_id, pht_date, pht_cost, pht_price, pht_discount, pht_notes)
values
(1, '2021-12-22 11:53:30', 400.99, 350.99, 30, null), 
(2, '2018-06-09 23:22:22', 1940.99, 120.49, null, null), 
(3, '2019-11-06 09:10:45', 358.89, 205.99, 40, null), 
(4, '2002-05-15 13:18:25', 396.99, 287.99, null, 'Pounds'), 
(5, '2016-07-06 18:18:18', 800.45, 600.99, 50, null);
GO 

Select * from product_hist;

-- -------------------------------------
-- DATA for table time
-- -------------------------------------
INSERT INTO time
(tim_yr, tim_qtr, tim_month, tim_week, tim_day, tim_time, tim_notes)
VALUES
(2014, 2, 6, 23, 3, '19:07:10', NULL),
(2012, 4, 11, 47, 3, '22:36:22', NULL),
(2013, 3, 9, 38, 4, '10:12:28', NULL),
(2014, 1, 2, 8, 2, '12:27:14', NULL),
(2010, 2, 4, 14, 5, '02:49:11', NULL),
(2008, 1, 1, 5, 4, '04:22:36', NULL),
(2001, 3, 7, 27, 2, '23:56:32', NULL),
(2011, 3, 8, 36, 1, '09:32:18', NULL),
(1999, 4, 12, 52, 5, '05:21:34', NULL),
(2010, 4, 12, 49, 4, '08:34:21', NULL),
(2008, 2, 5, 19, 7, '11:59:59', NULL);
GO

Select * from dbo.time;

-- -------------------------------------
-- DATA for table sale
-- -------------------------------------
INSERT INTO sale
(pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes)
VALUES
(1, 2, 3, 11, 10, 11.99, 119.9, NULL),
(1, 4, 2, 10, 6, 11.99, 71.94, NULL),
(2, 3, 1, 9, 15, 18.99, 284.85, NULL),
(3, 1, 4, 8, 30, 3.99, 119.7, NULL),
(4, 3, 2, 7, 5, 5.99, 29.95, NULL),
(5, 2, 5, 6, 10, 9.99, 199.8, NULL),
(5, 1, 2, 4, 6, 11.99, 71.94, NULL),
(4, 2, 1, 5, 15, 18.99, 284.85, NULL),
(3, 3, 4, 1, 30, 3.99, 119.7, NULL),
(2, 4, 1, 2, 5, 5.99, 29.95, NULL),
(1, 5, 5, 3, 20, 9.99, 199.8, NULL),
(2, 2, 1, 1, 30, 3.99, 119.7, NULL),
(3, 1, 5, 2, 5, 5.99, 29.95, NULL),
(5, 5, 2, 3, 20, 9.99, 199.8, NULL),
(3, 2, 5, 6, 10, 9.99, 199.8, NULL),
(2, 1, 2, 4, 6, 11.99, 71.94, NULL),
(1, 2, 1, 5, 15, 18.99, 284.85, NULL),
(5, 3, 4, 1, 30, 3.99, 119.7, NULL),
(4, 4, 2, 2, 5, 5.99, 29.95, NULL),
(3, 4, 2, 10, 6, 11.99, 71.94, NULL),
(1, 3, 1, 9, 15, 18.99, 284.85, NULL),
(2, 1, 4, 8, 30, 3.99, 119.7, NULL),
(5, 3, 5, 7, 5, 5.99, 29.95, NULL),
(3, 2, 1, 5, 15, 18.99, 284.85, NULL),
(1, 3, 4, 1, 30, 3.99, 119.7, NULL);
GO

Select * from dbo.sale;

-- -------------------------------------
-- DATA srp_hist
-- -------------------------------------
insert into dbo.srp_hist
(per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_ytd_sales, sht_ytd_comm, sht_notes)
values
(1, 'i', getDate(), system_user, getDate(), 856000, 910000, 25000, null), 
(2, 'i', getDate(), system_user, getDate(), 768000, 875000, 22500, null), 
(3, 'u', getDate(), system_user, getDate(), 956000, 985000, 27500, null), 
(4, 'u', getDate(), original_login(), getDate(), 876000, 820000, 28000, null), 
(5, 'i', getDate(), original_login(), getDate(), 436000, 530000, 30000, null);
GO 

Select * from srp_hist;