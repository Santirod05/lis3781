> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management.

## Rafael Rodriguez

### Assingment 5 Requirements:

*Deliverables:*
*Three Parts:*

1. Use of MS SQL Server to create high volume Database
2. Use of Salt and Hash with SHA 512 method.
3. Deliverable of Entity Relational Diagram

#### README.md file should include the following items:
 
- Assignment requirements:
- Screenshot of Assingment 5 ERD;
- Screenshot: At least *one* required report (i.e., exercise below), and SQL code solution. 
    
#### Assignment Screenshot and Links:

#### Screenshot of Assingment 5 ERD:

![Screenshot of lAssingment 5 ERD](img/a5_erd.png "Assingment 5 ERD screenshot")

#### Screenshot: At least *one* required report:
 
![required report Screenshot](img/report.png "required report Screenshot")

#### Screenshot of SQL code solution.:

![Screenshot of SQL code solution.](img/a5_sql.png "Screenshot of SQL code solution.")
