> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 Advanced Database Management.

## Rafael Rodriguez

### Assignment #4 Requirements:

*Deliverables:*
*Three Parts:*

1. Use of MS SQL Server to create high volume Database
2. Use of Salt and Hash with SHA 512 method.
3. Deliverable of Entity Relational Diagram



#### README.md file should include the following items:

* Screenshot of Assingment 4 ERD.
* MsSQL code for Assingment 4 Database.

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

#### Assignment #4 ERD 

![P1 ERD](img/a4_erd.png)

#### Assignment #4 mySQL code for reports 

| MsSQL code 1 | MsSQL code 2 |
| ------------ | ------------ |
| ![mySQL Code 1](img/sql_code.png) | ![mySQL Code 2](img/sql_code2.png) |

