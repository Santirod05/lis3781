-- -------------------------------------
-- REPORTS 1
-- -------------------------------------
if OBJECT_ID (N'dbo.v_paid_invoice_total', N'V') is not null
drop view dbo.v_paid_invoice_total;
go

create view dbo.v_paid_invoice_total as
    select p.per_id, per_fname, per_lname, sum(inv_total) as sum_total, format(sum(inv_total), 'C', 'en-us') as paid_invoice_total
    from dbo.person p
        join dbo.customer c on p.per_id=c.per_id
        join dbo.contact ct on c.per_id=ct.per_cid
        join dbo.[order] o on ct.cnt_id=o.cnt_id
        join dbo.invoice i on o.ord_id=i.ord_id
    where inv_paid != 0
    group by p.per_id, per_fname, per_lname
go

select per_id, per_fname, per_lname, paid_invoice_total from dbo.v_paid_invoice_total order by sum_total desc;

-- -------------------------------------
-- REPORTS 2
-- -------------------------------------
if OBJECT_ID (N'dbo.sp_all_customers_outstanding_balances', N'P') is not null
drop proc dbo.sp_all_customers_outstanding_balances;
go

create proc dbo.sp_all_customers_outstanding_balances as
begin
    select p.per_id, per_fname, per_lname, sum(pay_amt) as total_paid, (inv_total - sum(pay_amt)) invoice_diff
    from person p
        join dbo.customer c on p.per_id=c.per_id
        join dbo.contact ct on c.per_id=ct.per_cid
        join dbo.[order] o on ct.cnt_id=o.cnt_id
        join dbo.invoice i on o.ord_id=i.ord_id
        join dbo.payment pt on i.inv_id=pt.inv_id
    group by p.per_id, per_fname, per_lname, inv_total
    order by invoice_diff desc;
end
go

exec dbo.sp_all_customers_outstanding_balances;

-- -------------------------------------
-- REPORTS 3
-- -------------------------------------
if OBJECT_ID (N'dbo.sp_populate_srp_hist_table', N'P') is not null
drop proc dbo.sp_populate_srp_hist_table;
go

create proc dbo.sp_populate_srp_hist_table as
begin
    insert into dbo.srp_hist
    (per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_ytd_sales, sht_ytd_comm, sht_notes)

    select per_id, 'i', getDate(), system_user,  getDate(), srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes
    from dbo.slsrep
end
go

delete from dbo.srp_hist;
exec dbo.sp_populate_srp_hist_table;

select * from dbo.srp_hist;

-- -------------------------------------
-- REPORTS 4
-- -------------------------------------
if OBJECT_ID(N'dbo.trg_sales_history_insert', N'TR') is not null
drop trigger dbo.trg_sales_history_insert
go

create trigger dbo.trg_sales_history_insert
on dbo.slsrep
after insert as
begin
    -- declare
    declare
    @per_id_v smallint,
    @sht_type_v char(1),
    @sht_modified_v date,
    @sht_modifier_v varchar(45),
    @sht_date_v date,
    @sht_yr_sales_goal_v decimal(8,2),
    @sht_ytd_sales_v decimal(8,2),
    @sht_ytd_comm_v decimal(7,2),
    @sht_notes_v varchar(255);

    select
    @per_id_v = per_id,
    @sht_type_v = 'i',
    @sht_modified_v = getDate(),
    @sht_modifier_v = system_user,
    @sht_date_v = getDate(),
    @sht_yr_sales_goal_v = srp_yr_sales_goal,
    @sht_ytd_sales_v = srp_ytd_sales,
    @sht_ytd_comm_v = srp_ytd_comm,
    @sht_notes_v = srp_notes
    from inserted;

    insert into dbo.srp_hist
    (per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_ytd_sales, sht_ytd_comm, sht_notes)
    values
    (@per_id_v, @sht_type_v, @sht_modified_v, @sht_modifier_v, @sht_date_v, @sht_yr_sales_goal_v, @sht_ytd_sales_v, @sht_ytd_comm_v, @sht_notes_v)
end
go

select * from slsrep;
select * from srp_hist;

insert into dbo.slsrep
(per_id, srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes)
values
(6, 98000, 43000, 8750, null);

select * from slsrep;
select * from srp_hist;

-- -------------------------------------
-- REPORTS 5
-- -------------------------------------
if OBJECT_ID(N'dbo.trg_product_history_insert', N'TR') is not null
drop trigger dbo.trg_product_history_insert
go

create trigger dbo.trg_product_history_insert
on dbo.product
after insert as
begin
    -- declare
    declare
    @pro_id_v smallint,
--    @phy_type_v char(1),
    @pht_modified_v date,
--    @pht_modifier_v varchar(45),
    @pht_cost_v decimal(7,2),
    @pht_price_v decimal(7,2),
    @pht_discount_v decimal(3,0),
    @pht_notes_v varchar(255);

    select
    @pro_id_v = pro_id,
    @pht_modified_v = getDate(),
    @pht_cost_v = pro_cost,
    @pht_price_v = pro_price,
    @pht_discount_v = pro_discount,
    @pht_notes_v = pro_notes
    from inserted;

    insert into dbo.product_hist
    (pro_id, pht_date, pht_cost, pht_price, pht_discount, pht_notes)
    values
    (@pro_id_v, @pht_modified_v, @pht_cost_v, @pht_price_v, @pht_discount_v, @pht_notes_v)
end
go

select * from product;
select * from product_hist;

insert into dbo.product
(ven_id, pro_name, pro_descript, pro_weight, pro_qoh, pro_cost, pro_price, pro_discount, pro_notes)
values
(4, 'cooking oil', null, 15, 19, 19.99, 28.99, null, 'gallons');

select * from product;
select * from product_hist;


-- -------------------------------------
-- REPORTS Extra Credit
-- -------------------------------------

if OBJECT_ID(N'dbo.sp_annual_salesrep_sales_goal', N'P') is not null
drop proc dbo.sp_annual_salesrep_sales_goal;
go

create proc dbo.sp_annual_salesrep_sales_goal as
begin
    update slsrep
    set srp_yr_sales_goal = sht_yr_sales_goal * 1.08
    from slsrep as sr 
        join srp_hist as sh 
        on sr.per_id= sh.per_id
    where sht_date = (select max(sht_date) from srp_hist)
end
go

select * from dbo.slsrep;
select * from dbo.srp_hist

exec dbo.sp_annual_salesrep_sales_goal;

select * from dbo.slsrep;


-- -------------------------------------
-- Hide SSN
-- -------------------------------------
if OBJECT_ID (N'dbo.CreatePersonSSN', N'P') is not null
drop proc dbo.CreatePersonSSN;
go

CREATE PROC dbo.CreatePersonSSN AS 
BEGIN 
    DECLARE @salt binary(64); 
    DECLARE @ran_num int; 
    DECLARE @ssn binary(64); 
    DECLARE @x INT, @y INT; 
    SET @x = 1; 

    -- dynamically set loop ending value (total number of persons) 
    SET @y=(select count(*) from dbo.person); 
    -- select @y; -- display number of persons (only for testing) 

    WHILE (@x <= @y) 
        BEGIN 
            SET @salt=CRYPT_GEN_RANDOM(64); -- salt includes unique random bytes for each user when looping 
            SET @ran_num=FLOOR(RAND()*(999999999-111111111+1))+111111111; -- random 9-digit SSN from 111111111 - 999999999, inclusive (see link below) 
            SET @ssn=HASHBYTES('SHA2_512', concat(@salt, @ran_num));  
            
            update dbo.person 
            set per_ssn=@ssn, per_salt=@salt 
            where per_id=@x; 

            SET @x = @x + 1; 
        END; 
END; 
go

select * from person;

exec dbo.CreatePersonSSN 

select * from person;